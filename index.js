const http = require('http');

const hostname = 'localhost';
const port = process.env.PORT || 443;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hola Mundo somos grupo 3');
});

server.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`El servidor se esta ejecutando en http://${hostname}:${port}/`);
});
